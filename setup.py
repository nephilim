# Nephilim installer script

from distutils.core import setup
import shutil

shutil.copyfile('nephilim.py', 'nephilim/nephilim')

setup(name         = 'nephilim',
      version      = '0.1',
      description  = 'Nephilim MPD client.',
      author       = 'Anton Khirnov',
      author_email = 'wyskas@gmail.com',
      classifiers  = [
          'Development Status :: 3 - Alpha',
          'Environment :: X11 Applications :: Qt',
          'Intended Audience :: End Users/desktop',
          'License :: OSI Approved :: GNU General Public License (GPL)',
          'Programming Language :: Python :: 2',
          'Topic :: Multimedia :: Sound/Audio :: Players'],
      packages     = ['nephilim', 'nephilim.plugins'],
      scripts      = ['nephilim/nephilim']
)
