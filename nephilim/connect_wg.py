#
#    Copyright (C) 2008 jerous <jerous@gmail.com>
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtWidgets, QtCore

class ConnectWidget(QtWidgets.QWidget):
    host_txt    = None
    port_txt    = None
    pass_txt    = None
    connect_btn = None
    mpclient    = None
    settings    = None

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.mpclient = QtWidgets.QApplication.instance().mpclient
        self.settings = QtCore.QSettings()

        self.host_txt = QtWidgets.QLineEdit(self.settings.value('MPD/host', 'localhost'))
        self.host_txt.setToolTip('Host')

        self.port_txt = QtWidgets.QLineEdit(self.settings.value('MPD/port', '6600'))
        self.port_txt.setValidator(QtGui.QIntValidator(1, 65535, self.port_txt))
        self.port_txt.setToolTip('Port')

        self.pass_txt = QtWidgets.QLineEdit(self.settings.value('MPD/password'))
        self.pass_txt.setEchoMode(QtWidgets.QLineEdit.Password)
        self.pass_txt.setToolTip('Password')

        self.connect_btn = QtWidgets.QPushButton('Connect')
        self.connect_btn.clicked.connect(self.connect_mpd)

        self.setLayout(QtWidgets.QGridLayout())
        self.layout().addWidget(QtWidgets.QLabel('host:port:password'), 0, 0, 1, 3, QtCore.Qt.AlignHCenter)
        self.layout().addWidget(self.host_txt, 1, 0)
        self.layout().addWidget(self.port_txt, 1, 1)
        self.layout().addWidget(self.pass_txt, 1, 2)
        self.layout().addWidget(self.connect_btn, 2, 0, 1, 3, QtCore.Qt.AlignHCenter)

        self.setWindowTitle('Connect to MPD')
        self.center()

        self.mpclient.connect_changed.connect(self.on_connected)

    def center(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size   = self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2+100)

    def monitor(self):
        self.show()
        self.activateWindow()
        self.raise_()
        if self.host_txt:
            self.connect_mpd()

    def connect_mpd(self):
        host  = str(self.host_txt.text())
        port  = int(self.port_txt.text()) if self.port_txt.text() else None
        passw = str(self.pass_txt.text())

        self.mpclient.connect_mpd(host, port, passw)

    def on_connected(self, val):
        if val:
            self.settings.setValue('MPD/host', self.host_txt.text())
            self.settings.setValue('MPD/port', self.port_txt.text())
            self.settings.setValue('MPD/password', self.pass_txt.text())
            self.hide()

