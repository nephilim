#
#    Copyright (C) 2008 jerous <jerous@gmail.com>
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtWidgets, QtCore
import os

from .common import Button
from . import plugin


class SettingsWidget(QtWidgets.QWidget):
    save_btn     = None
    close_btn    = None
    pluginlist   = None
    settings     = None
    settings_wg  = None

    mpclient   = None
    plugins    = None

    class SettingsWidgetMPD(plugin.Plugin.SettingsWidget):
        mpclient = None
        host_txt = None
        port_txt = None
        pass_txt = None
        lib_txt  = None

        def __init__(self, mpclient):
            plugin.Plugin.SettingsWidget.__init__(self, None)
            self.mpclient = mpclient

            self.settings.beginGroup('MPD')
            self.host_txt = QtWidgets.QLineEdit(self.settings.value('host', 'localhost'))
            self.port_txt = QtWidgets.QLineEdit(self.settings.value('port', '6600'))
            self.port_txt.setValidator(QtGui.QIntValidator(0, 65535, self))
            self.pass_txt = QtWidgets.QLineEdit(self.settings.value('password'))
            self.pass_txt.setEchoMode(QtWidgets.QLineEdit.Password)
            self.lib_txt  = QtWidgets.QLineEdit(self.settings.value('music_dir', os.path.expanduser('~/music/')))
            self.settings.endGroup()

            self.setLayout(QtWidgets.QVBoxLayout())
            self._add_widget(self.host_txt, 'Host', 'Host or socket to connect to')
            self._add_widget(self.port_txt, 'Port', 'Port to use (empty when using sockets)')
            self._add_widget(self.pass_txt, 'Password', 'Password')
            self._add_widget(self.lib_txt,  'Music library', 'Path to music library')

        def save_settings(self):
            reconnect = False

            self.settings.beginGroup('MPD')
            if self.host_txt.text() != self.settings.value('host'):
                self.settings.setValue('host', self.host_txt.text())
                reconnect = True
            if self.port_txt.text() != self.settings.value('port'):
                self.settings.setValue('port', self.port_txt.text())
                reconnect = True
            if self.pass_txt.text() != self.settings.value('password'):
                self.settings.setValue('password', self.pass_txt.text())
                if self.pass_txt.text():
                    self.mpclient.password(self.pass_txt.text())
            self.settings.setValue('music_dir', self.lib_txt.text())
            self.settings.endGroup()

            if reconnect:
                self.mpclient.disconnect_mpd()
                self.mpclient.connect_mpd(self.host_txt.text(), int(self.port_txt.text()), self.pass_txt.text())

    def __init__(self, mpclient, plugins):
        QtWidgets.QWidget.__init__(self, None, QtCore.Qt.Window)
        self.settings = QtCore.QSettings()
        self.plugins  = plugins
        self.mpclient = mpclient

        self.save_btn  = Button('save all', self.save_clicked)
        self.close_btn = Button('close',    self.close_clicked)

        tab_wg = QtWidgets.QTabWidget(self)
        self.settings_wg = []
        self.settings_wg.append(self.SettingsWidgetMPD(mpclient))
        tab_wg.addTab(self.settings_wg[-1], 'MPD settings')

        self.pluginlist = QtWidgets.QListWidget(self)
        self.fill_pluginlist()
        tab_wg.addTab(self.pluginlist, 'Plugins')

        for plugin in self.plugins.loaded_plugins():
            wg = plugin.get_settings_widget()
            if wg:
                self.settings_wg.append(wg)
                tab_wg.addTab(self.settings_wg[-1], plugin.name)

        self.setLayout(QtWidgets.QGridLayout())
        self.layout().addWidget(tab_wg,         0, 0, 1, 2)
        self.layout().addWidget(self.save_btn,  1, 0)
        self.layout().addWidget(self.close_btn, 1, 1)

        self.pluginlist.itemChanged.connect(self.plugin_checked)

        self.setWindowTitle('Settings')
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.center()
        self.resize(800,400)

    def fill_pluginlist(self):
        self.pluginlist.clear()
        for plugin in self.plugins.plugins():
            item = QtWidgets.QListWidgetItem("%s\t%s"%(plugin.name, plugin.info))
            if plugin.loaded:
                item.setCheckState(QtCore.Qt.Checked)
            else:
                item.setCheckState(QtCore.Qt.Unchecked)

            self.pluginlist.addItem(item)

    def center(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size   = self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2+100)

    def save_clicked(self):
        for wg in self.settings_wg:
            wg.save_settings()

    def close_clicked(self):
        self.close()

    def plugin_checked(self, item):
        toload = int(item.checkState() == QtCore.Qt.Checked)
        name   = str(item.text()[0:str(item.text()).find('\t')])
        if toload:
            # refresh the plugin file
            self.plugins.load(name)
            self.fill_pluginlist()
        else:
            self.plugins.unload(name)
        self.settings.setValue(name + '/load', toload)

    def closeEvent(self, event):
        self.hide()
        event.ignore()
