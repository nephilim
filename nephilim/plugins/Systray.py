#
#    Copyright (C) 2008 jerous <jerous@gmail.com>
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtWidgets, QtCore

from ..plugin import Plugin
from ..common import sec2min, APPNAME, appIcon, expand_tags
from ..       import icons

class Systray(Plugin):
    # public, const
    info = 'Provides the systray icon.'

    # public, read-only
    o         = None

    # private
    format    = None
    eventObj  = None
    DEFAULTS  = {'format': '${track} - ${title} by ${artist} on ${album} (${length})'}

    def _load(self):
        self.format = self.settings.value(self.name + '/format')
        class SystrayWheelEventObject(QtCore.QObject):
            """This class listens for systray-wheel events"""
            def eventFilter(self, object, event):
                if type(event)==QtGui.QWheelEvent:
                    numDegrees=event.angleDelta().y() / 8
                    numSteps=5*numDegrees/15
                    self.plugin.mpclient.set_volume(self.plugin.mpclient.status['volume'] + numSteps)
                    event.accept()
                    return True
                return False

        self.o = QtWidgets.QSystemTrayIcon(QtGui.QIcon(appIcon), self.parent())
        self.eventObj=SystrayWheelEventObject()
        self.eventObj.plugin = self
        self.o.installEventFilter(self.eventObj)
        self.o.activated.connect(self.onSysTrayClick)

        self.mpclient.song_changed.connect(self.update)
        self.o.show()
    def _unload(self):
        self.mpclient.song_changed.disconnect(self.update)
        self.o.hide()
        self.o.setIcon(QtGui.QIcon(None))
        self.o = None
        self.parent()._wheelEvent = None

    def update(self):
        status = self.mpclient.status
        if not status:
            return

        values = {'state':''}
        values['state']={'play':'playing', 'stop':'stopped', 'pause':'paused'}[status['state']]
        if 'time' in status:
            values['length'] = sec2min(status['time'][0])
            values['time']   = sec2min(status['time'][1])

        song = self.mpclient.cur_song
        if song:
            self.o.setToolTip(expand_tags(self.format, (song,)))
        else:
            self.o.setToolTip('%s not playing'%APPNAME)

    def onSysTrayClick(self, reason):
        if reason == QtGui.QSystemTrayIcon.Trigger or\
           reason == QtGui.QSystemTrayIcon.Context:
            w = self.parent()
            if w.isVisible():
                w.setVisible(False)
            else:
                w.setVisible(True)
        elif reason == QtGui.QSystemTrayIcon.MiddleClick:
            if self.mpclient.is_playing():
                self.mpclient.pause()
            else:
                self.mpclient.resume()

    class SettingsWidgetSystray(Plugin.SettingsWidget):
        format = None

        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)

            self.format = QtWidgets.QLineEdit(self.settings.value(self.plugin.name + '/format'))

            self.setLayout(QtWidgets.QVBoxLayout())
            self._add_widget(self.format, 'Tooltip format')

        def save_settings(self):
            self.settings.beginGroup(self.plugin.name)
            self.settings.setValue('format', self.format.text())
            self.settings.endGroup()

    def get_settings_widget(self):
        return self.SettingsWidgetSystray(self)

    def set_enabled(self, val):
        pass
