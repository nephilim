#
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtWidgets, QtCore

from ..common import Button
from ..plugin import Plugin
from ..       import icons

class wgPlayControl(QtWidgets.QToolBar):
    """Displays controls for interacting with playing, like play, volume ..."""
    " control buttons"
    play_icon    = None
    pause_icon   = None
    play_action  = None
    stop_action  = None
    prev_action  = None
    next_action  = None
    vol_slider   = None
    repeat       = None
    random       = None
    single       = None
    consume      = None
    outputs_menu = None
    p            = None
    logger       = None
    xfade        = None

    class VolumeSlider(QtWidgets.QSlider):

        def __init__(self, parent):
            QtWidgets.QSlider.__init__(self, parent)
            self.setOrientation(parent.orientation())
            self.setMaximum(100)
            self.setToolTip('Volume control')

        def paintEvent(self, event):
            painter = QtGui.QPainter(self)
            painter.eraseRect(self.rect())

            grad = QtGui.QLinearGradient(0, 0, self.width(), self.height())
            grad.setColorAt(0, self.palette().color(QtGui.QPalette.Window))
            grad.setColorAt(1, self.palette().color(QtGui.QPalette.Highlight))
            if self.orientation() == QtCore.Qt.Horizontal:
                rect = QtCore.QRect(0, 0, self.width() * self.value() / self.maximum(), self.height())
            else:
                rect = QtCore.QRect(0, self.height() * (1 - float(self.value()) / self.maximum()), self.width(), self.height())
            painter.fillRect(rect, QtGui.QBrush(grad))

    def __init__(self, p, parent = None):
        QtWidgets.QToolBar.__init__(self, p.name, parent)
        self.setMovable(True)
        self.p      = p
        self.logger = p.logger
        self.setObjectName(p.name)

        status = self.p.mpclient.status

        self.play_icon   = QtGui.QIcon(':icons/media-playback-start.svg')
        self.pause_icon  = QtGui.QIcon(':icons/media-playback-pause.svg')

        self.play_action = self.addAction(self.play_icon, 'play', self.on_play_click)
        self.stop_action = self.addAction(QtGui.QIcon(':icons/media-playback-stop.svg'),     'stop', self.on_stop_click)
        self.prev_action = self.addAction(QtGui.QIcon(':icons/media-skip-backward.svg'), 'previous', self.on_prev_click)
        self.next_action = self.addAction(QtGui.QIcon(':icons/media-skip-forward.svg'),      'next', self.on_next_click)
        self.addSeparator()

        self.vol_slider = self.VolumeSlider(self)
        self.vol_slider.setSliderPosition(status['volume'])
        self.vol_slider.valueChanged.connect(self.onVolumeSliderChange)
        self.addWidget(self.vol_slider)
        self.addSeparator()

        self.random = self.addAction(QtGui.QIcon(':icons/media-playlist-shuffle.svgz'), 'random')
        self.random.setCheckable(True)
        self.random.setChecked(status['random'])
        self.random.toggled.connect(self.p.mpclient.random)
        self.p.mpclient.random_changed.connect(self.random.setChecked)

        self.repeat = self.addAction(QtGui.QIcon(':icons/media-playlist-repeat.svg'), 'repeat')
        self.repeat.setCheckable(True)
        self.repeat.setChecked(status['repeat'])
        self.p.mpclient.repeat_changed.connect(self.repeat.setChecked)
        self.repeat.toggled.connect(self.p.mpclient.repeat)

        self.single = self.addAction(QtGui.QIcon(':icons/single.png'), 'single mode')
        self.single.setCheckable(True)
        self.single.setChecked(status['single'])
        self.p.mpclient.single_changed.connect(self.single.setChecked)
        self.single.toggled.connect(self.p.mpclient.single)

        self.consume = self.addAction(QtGui.QIcon(':icons/consume.png'), 'consume mode')
        self.consume.setCheckable(True)
        self.consume.setChecked(status['consume'])
        self.p.mpclient.consume_changed.connect(self.consume.setChecked)
        self.consume.toggled.connect(self.p.mpclient.consume)

        self.xfade = QtWidgets.QSpinBox(self)
        self.xfade.setValue(self.p.mpclient.status['xfade'])
        self.p.mpclient.crossfade_changed.connect(self.xfade.setValue)
        self.xfade.valueChanged.connect(self.p.mpclient.crossfade)
        self.xfade.setToolTip('Set crossfade between songs in seconds.')
        self.addWidget(self.xfade)

        self.outputs_menu = QtWidgets.QMenu('Audio outputs')
        outputs           = self.addAction(QtGui.QIcon(':icons/outputs.png'), 'Audio outputs')
        outputs.triggered.connect(lambda : self.outputs_menu.popup(QtGui.QCursor.pos()))
        self.__update_outputs()
        self.p.mpclient.connect_changed.connect(self.__update_outputs)


        self.orientationChanged.connect(self.vol_slider.setOrientation)

        self.p.mpclient.state_changed.connect(self.onStateChange)
        self.p.mpclient.volume_changed.connect(self.onVolumeChange)

    def __update_outputs(self):
        self.outputs_menu.clear()
        for output in self.p.mpclient.outputs:
            act = self.outputs_menu.addAction(output.name)
            act.setCheckable(True)
            act.setChecked(output.state)
            act.toggled.connect(output.set_state)
            output.state_changed.connect(act.setChecked)

    def onStateChange(self, new_state):
        status = self.p.mpclient.status

        if new_state == 'play':
            self.play_action.setIcon(self.pause_icon)
            self.play_action.setToolTip('pause')
        elif new_state == 'pause' or new_state == 'stop':
            self.play_action.setIcon(self.play_icon)
            self.play_action.setToolTip('play')

    def onVolumeChange(self, new_vol):
        self.vol_slider.setValue(new_vol)

    def on_play_click(self):
        status=self.p.mpclient.status
        if status['state']=='play':
            self.logger.info('Toggling playback')
            self.p.mpclient.pause()
        elif status['state']=='stop':
            self.logger.info('Pausing playback')
            self.p.mpclient.play(None)
        else:
            self.p.mpclient.resume()
    def on_stop_click(self):
        self.logger.info('Stopping playback')
        self.p.mpclient.stop()
    def on_prev_click(self):
        self.logger.info('Playing previous')
        self.p.mpclient.previous()
    def on_next_click(self):
        self.logger.info('Playing next')
        self.p.mpclient.next()
    def onVolumeSliderChange(self, val):
        self.p.mpclient.set_volume(val)

class PlayControl(Plugin):
    # public, const
    info = 'Controls playback.'

    # public, read-only
    o = None

    def _load(self):
        self.o = wgPlayControl(self, None)
        QtWidgets.QApplication.instance().main_win.addToolBar(QtCore.Qt.TopToolBarArea, self.o)
    def _unload(self):
        QtWidgets.QApplication.instance().main_win.removeToolBar(self.o)
        self.o = None
