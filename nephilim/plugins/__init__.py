#
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#


import sys
import logging

__all__ = ['AlbumCover', 'Filebrowser', 'Library', 'Lyrics', 'Notify', 'PlayControl',
           'Playlist', 'Songinfo', 'Systray']

class Plugins:
    _plugins   = None
    parent = None
    mpclient   = None

    def __init__(self, parent, mpclient):
        """load all modules in the plugins directory."""
        self._plugins = {}
        self.parent   = parent
        self.mpclient = mpclient

        for name in __all__:
            self.__init_plugin(name)

    def __init_plugin(self, name):
        try:
            if name in sys.modules:
                reload(sys.modules[name])
                module = sys.modules[name]
            else:
                module = __import__(name, globals(), locals(), [], 1)
        except (SyntaxError, ImportError) as e:
            logging.error('Failed to initialize plugin %s: %s.'%(name, e))
            return False

        self._plugins[name] = eval('module.%s(self.parent, self.mpclient, \'%s\')'%(name, name))
        return True

    def plugin(self, name):
        """Returns a plugin with the specified name. It is illegal to access
           any functions if the plugin is unloaded."""
        return self._plugins[name] if name in self._plugins else None

    def load(self, name):
        """Loads an unloaded plugin."""
        if not name in self._plugins:
            if not self.__init_plugin(name):
                return False

        self._plugins[name].load()
        return True

    def unload(self, name):
        """Unloads a loaded plugin."""
        if name in self._plugins:
            if self._plugins[name].loaded:
                self._plugins[name].unload()

    def plugins(self):
        """Returns all available plugins. It is illegal to access any functions
           if the plugin is unloaded."""
        return self._plugins.values()

    def loaded_plugins(self):
        """Returns all loaded plugins."""
        list = []
        for plugin in self._plugins.values():
            if plugin.loaded:
                list.append(plugin)
        return list
