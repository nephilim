#
#    Copyright (C) 2008 jerous <jerous@gmail.com>
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtWidgets, QtCore

from ..common import sec2min, APPNAME, expand_tags
from ..plugin import Plugin
from ..       import plugins

NOTIFY_PRIORITY_SONG = 1
NOTIFY_PRIORITY_VOLUME = 2

class winNotify(QtWidgets.QWidget):
    parent  = None
    p        = None

    _current_priority = 0

    timer       = None
    cover_label = None
    text_label  = None

    def __init__(self, p):
        QtWidgets.QWidget.__init__(self, p.parent())
        self.p      = p
        self.parent = p.parent()

        self.timer = QtCore.QTimer(self)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self._hide)

        layout = QtWidgets.QHBoxLayout()
        self.cover_label = QtWidgets.QLabel()
        self.text_label  = QtWidgets.QLabel()
        self.text_label.setWordWrap(True)

        self.setLayout(QtWidgets.QHBoxLayout())
        self.layout().addWidget(self.cover_label)
        self.layout().addWidget(self.text_label)

        self.setWindowFlags(QtCore.Qt.ToolTip | QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowOpacity(0.7)

        font = QtGui.QFont()
        font.setPixelSize(20)
        self.setFont(font)

        ac = QtWidgets.QApplication.instance().plugins.plugin('AlbumCover')
        if ac:
            ac.cover_changed.connect(self.on_cover_changed)

    def on_cover_changed(self, cover):
        if cover.isNull():
            self.cover_label.clear()
            return

        self.cover_label.setPixmap(cover.scaledToHeight(self.fontInfo().pixelSize()*4))
        self.resize(self.layout().sizeHint())

    def mousePressEvent(self, event):
        self.hide()

    def show(self, text, time = 3000, priority = 0):
        if not priority >= self._current_priority:
            return
        self._current_priority = priority

        self.text_label.setText(text)
        self.resize(self.layout().sizeHint())
        self.centerH()
        self.setVisible(True)
        self.timer.start(time)

    def _hide(self):
        self.setHidden(True)
        self._current_priority = -1

    def centerH(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2, 100)

class Notify(Plugin):
    # public, const
    info = 'Show notifications in a popup window.'

    # public, read-only
    o = None

    DEFAULTS  = {'songformat' : '${track} - ${artist} - ${title} (${album}) [${length}]',
                 'timer'      : 3000}

    def _load(self):
        self.o = winNotify(self)
        self.mpclient.song_changed.connect(self.onSongChange)
        self.mpclient.connect_changed.connect(self.on_connect_changed)
        self.mpclient.state_changed.connect(self.onStateChange)
        self.mpclient.volume_changed.connect (self.onVolumeChange)
    def _unload(self):
        self.o=None
        self.mpclient.song_changed.disconnect(self.onSongChange)
        self.mpclient.connect_changed.disconnect(self.on_connect_changed)
        self.mpclient.state_changed.disconnect(self.onStateChange)
        self.mpclient.volume_changed.disconnect(self.onVolumeChange)
    def onSongChange(self):
        song = self.mpclient.cur_song
        if not song:
            return
        self.settings.beginGroup(self.name)
        self.o.show(expand_tags(self.settings.value('songformat'), (song,)), int(self.settings.value('timer')), NOTIFY_PRIORITY_SONG)
        self.settings.endGroup()

    def on_connect_changed(self, val):
        self.o.show('%s.'%('Connected' if val else 'Disconnected'), int(self.settings.value(self.name + '/timer')))

    def onStateChange(self, new_state):
        self.o.show(new_state, int(self.settings.value(self.name + '/timer')))

    def onVolumeChange(self, new_vol):
        self.o.show('Volume: %i%%'%(new_vol), int(self.settings.value(self.name + '/timer')), priority = NOTIFY_PRIORITY_VOLUME)

    class SettingsWidgetNotify(Plugin.SettingsWidget):
        format = None
        timer  = None

        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)
            self.settings.beginGroup(self.plugin.name)

            self.format = QtWidgets.QLineEdit(self.settings.value('songformat'))

            self.timer = QtWidgets.QLineEdit(self.settings.value('timer'))
            self.timer.setValidator(QtGui.QIntValidator(self.timer))

            self.setLayout(QtWidgets.QVBoxLayout())
            self._add_widget(self.format, 'Format', 'Format of notifications. All tags supported by MPD\n'
                                                    'will be expanded to their values for current song,\n'
                                                    'e.g. ${track}, ${title}, ${artist}, ${album}, ${length}, ${date}, etc.')
            self._add_widget(self.timer, 'Duration', 'How long should notifications be displayed in ms.')
            self.settings.endGroup()

        def save_settings(self):
            self.settings.beginGroup(self.plugin.name)
            self.settings.setValue('songformat', self.format.text())
            self.settings.setValue('timer',      int(self.timer.text()))
            self.settings.endGroup()
            self.plugin.onSongChange()

    def get_settings_widget(self):
        return self.SettingsWidgetNotify(self)

