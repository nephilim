#
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtWidgets, QtCore
from   string import Template

from .main_window import MainWindow
from .common      import ORGNAME, APPNAME, appIcon
from .mpclient    import MPClient
from .settings_wg import SettingsWidget
from .connect_wg  import ConnectWidget
from . import plugins
from . import icons

class NephilimApp(QtWidgets.QApplication):
    #### PUBLIC ####
    # those don't change while the program is running
    """main window object"""
    main_win     = None
    """"MPD layer"""
    mpclient     = None
    """"plugins interface"""
    plugins      = None
    """settings object"""
    settings     = None

    #### PRIVATE ####
    """settings window"""
    _settings_win = None
    """connection window"""
    _connect_win  = None

    #### PUBLIC ####
    def __init__(self, argv):
        QtWidgets.QApplication.__init__(self, argv)

        self.setApplicationName(APPNAME)
        self.setOrganizationName(ORGNAME)
        self.setWindowIcon(QtGui.QIcon(appIcon))

    def __str__(self):
        return APPNAME

    def exec_(self):
        #init MPD layer
        self.mpclient = MPClient(self)

        #init settings
        self.settings = QtCore.QSettings(self)

        #init connection window
        self._connect_win = ConnectWidget()

        #init main window
        self.main_win = MainWindow(self.mpclient)

        #init plugins
        show_settings = False # are there new plugins?
        self.plugins  = plugins.Plugins(self.main_win, self.mpclient)
        for plugin in self.plugins.plugins():
            if self.settings.value(plugin.name + '/load') == None:
                show_settings = True
            if int(self.settings.value(plugin.name + '/load', 0)):
                self.plugins.load(plugin.name)

        self.aboutToQuit.connect(self._cleanup)


        if show_settings:
            self.show_settings_win()
        self.main_win.restore_layout()
        self._connect_win.monitor()

        QtWidgets.QApplication.exec_()


    def show_settings_win(self):
        if not self._settings_win:
            self._settings_win = SettingsWidget(self.mpclient, self.plugins)

        self._settings_win.show()
        self._settings_win.raise_()

    def show_connect_win(self):
        self._connect_win.monitor()

    def expand_tags(self, str):
        replacements = {'musicdir' : self.settings.value('MPD/music_dir')}
        ret = Template(str)
        ret = ret.safe_substitute(replacements)
        return ret

    #### PRIVATE ####
    def _cleanup(self):
        # unload all plugins
        for plugin in self.plugins.loaded_plugins():
            plugin.unload()

        self.main_win.on_quit()
        self.settings.sync()
