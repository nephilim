#
#    Copyright (C) 2008 jerous <jerous@gmail.com>
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtWidgets, QtCore
import logging

from . import plugins

class Plugin(QtCore.QObject):
    # public, const
    name        = None
    info        = None
    logger      = None
    mpclient    = None
    settings    = None

    # public, read-only
    loaded       = False
    dock_widget  = None
    _parent      = None

    o            = None
    DEFAULTS     = {}

    def __init__(self, parent, mpclient, name):
        QtCore.QObject.__init__(self)
        self.name      = name
        self.mpclient  = mpclient
        self._parent    = parent
        self.loaded    = False
        self.settings  = QtCore.QSettings()

        self.logger     = logging.getLogger(self.name)

        #init settings
        self.logger.debug('Initializing default settings.')
        self.settings.beginGroup(name)
        for key in self.DEFAULTS:
            if not self.settings.contains(key):
                self.settings.setValue(key, self.DEFAULTS[key])
        self.settings.endGroup()

    def parent(self):
        return self._parent

    def load(self):
        self.logger.info('loading')
        self._load()
        opts = QtWidgets.QDockWidget.DockWidgetClosable|QtWidgets.QDockWidget.DockWidgetMovable
        self.dock_widget = self._get_dock_widget()
        if self.dock_widget:
            self.dock_widget.setFeatures(opts)
        QtWidgets.QApplication.instance().main_win.add_dock(self.dock_widget)
        QtWidgets.QApplication.instance().main_win.restore_layout()
        self.mpclient.connect_changed.connect(self.set_enabled)
        self.loaded = True
    def unload(self):
        if not self.loaded:
            return
        self.logger.info("unloading")
        self._unload()
        if self.dock_widget:
            QtWidgets.QApplication.instance().main_win.remove_dock(self.dock_widget)
        self.dock_widget   = None
        self.settingsWidget = None
        self.mpclient.connect_changed.disconnect(self.set_enabled)
        self.loaded         = False
    def set_enabled(self, val):
        if self.o:
            self.o.setEnabled(val)

    class SettingsWidget(QtWidgets.QWidget):
        """ plugins should subclass this"""
        plugin   = None
        settings = None

        def __init__(self, plugin):
            QtWidgets.QWidget.__init__(self)

            self.plugin    = plugin
            self.settings = QtCore.QSettings()

        def settings(self):
            return self.settings

        def savesettings(self):
            """ reimplement this"""
            pass

        def _add_widget(self, widget, label = '', tooltip = ''):
            """adds a widget with label"""
            if self.layout() is None:
                raise ValueError('Attempted to call add_widget with no layout set.')

            widget.setToolTip(tooltip)
            layout = QtWidgets.QHBoxLayout()
            layout.addWidget(QtWidgets.QLabel(label))
            layout.addWidget(widget)
            self.layout().addLayout(layout)

    def get_settings_widget(self):
        """Should return subclassed SettingsWidget."""
        return

    def _get_dock_widget(self):
        """Override this one."""
        return None
    def _create_dock(self, widget):
        """Creates a QDockWidget with _parent $_parent containing widget $widget."""
        dock=QtWidgets.QDockWidget(self.name, QtWidgets.QApplication.instance().main_win)
        dock.setObjectName(self.name)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.AllDockWidgetAreas)

        return dock
    def _load(self):
        """Override this one."""
        return
    def _unload(self):
        """Override this one."""
        return

