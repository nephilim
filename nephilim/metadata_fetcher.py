#
#    Copyright (C) 2010 Anton Khirnov <anton@khirnov.net>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtCore, QtNetwork
from PyQt5.QtCore import pyqtSignal as Signal

from .song import Song

class MetadataFetcher(QtCore.QObject):
    """
    A basic class for metadata fetchers. Provides a fetch(song) function,
    emits a finished(song, metadata) signal when done.
    """
    #public, read-only
    logger = None
    name   = ''

    #private
    nam  = None  # NetworkAccessManager
    rep = None   # current NetworkReply.
    song = None  # current song

    # SIGNALS
    finished = Signal([Song, object])

    #### private ####
    def __init__(self, plugin):
        QtCore.QObject.__init__(self, plugin)

        self.nam = QtNetwork.QNetworkAccessManager()
        self.logger = plugin.logger

    def fetch2(self, song, url):
        """A private convenience function to initiate fetch process."""
        # abort any existing connections
        self.abort()
        self.song = song

        self.logger.info('Searching %s: %s.'%(self. name, url.toString()))
        self.rep = self.nam.get(QtNetwork.QNetworkRequest(url))
        self.rep.error.connect(self.handle_error)

    def finish(self, metadata = None):
        """A private convenience function to clean up and emit finished().
           Feel free to reimplement/not use it."""
        self.rep = None
        self.finished.emit(self.song, metadata)
        self.song = None

    def handle_error(self):
        """Print the error and abort."""
        self.logger.error(self.rep.errorString())
        self.abort()
        self.finish()

    #### public ####
    def fetch(self, song):
        """Reimplement this in subclasses."""
        pass

    def abort(self):
        """Abort all downloads currently in progress."""
        if self.rep:
            self.rep.blockSignals(True)
            self.rep.abort()
            self.rep = None
