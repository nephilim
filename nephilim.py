#!/usr/bin/python3

# nephilim.py

#
#    Copyright (C) 2008 jerous <jerous@gmail.com>
#    Copyright (C) 2009 Anton Khirnov <wyskas@gmail.com>
#
#    Nephilim is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Nephilim is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Nephilim.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import logging
from traceback import print_exc
from optparse  import OptionParser
try:
    from PyQt5     import QtGui
except ImportError:
    sys.exit('PyQt5 not found. Ensure that it is installed.')

from nephilim.nephilim_app import NephilimApp

def main():
    # parse cmdline options
    parser = OptionParser()
    parser.add_option('-v', '--verbose', default = 'warning', choices = ['debug', 'info',
                      'warning', 'error', 'critical'], help = 'Set verbosity level.')

    options, args = parser.parse_args()
    logging.basicConfig(level = logging.__getattribute__(options.verbose.upper()))

    try:
        app = NephilimApp(sys.argv)

        app.exec_()
        app.quit()
    except:
        print_exc()

if __name__ == "__main__":
    main()
